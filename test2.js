// Developed By Nijiko Yonskai
// JSON Schema Validator for API w/ a middleware for express
// Copyright 2013

var Validator = require('./validator');

var schema = {
	project_id: {
		type: Number,
		required: true
	},
	name: {
		type: String,
		required: true,
		length: {
			min: 1,
			max: 100
		}
	},
	type_id: {
		type: Number,
		required: true,
		test: /[123]/
	},
	order_id: {
		type: Number
	}
};

// Create our "Express" system, no middleware support though
var system = function (options) {
  this.options = options || {};
};

system.prototype.param = function (item) {
  return this.options[item];
};

var route = function (req, res) {
  var validator = new Validator(schema);

	// Turn on values in error messages
	// validator.debug = true;

	console.log(req.param('message'));
	console.log(validator.checkAndErrorRespond(req.param('category'), console.log));
};

// Impersonate requests

route(new system({
  message: 'Test:',
  category: {
    type_id: 10,
    name: 'Alexander',
    project_id: 10,
  }
}));